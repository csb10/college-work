<!DOCTYPE html>
<?php
//FILE TO DISPLAY INFORMATION ABOUT THE COMPANIES HISTORY
session_start();

?>
<html>
    <head>
        <title>About us</title>
    </head>
    <body>
        <?include_once('navigation.php');?>
        <h1>About Cravendale Travel</h1>
        <p> Cravendale Travel has an illustrious history that spans over 30 years, with our roots lying in the commercial aircraft company Channel Express (Air Services).</p>
        <p> Our name change to the much snappier Cravendale Travel occurred just a few months before our first leisure flight in February 2003.</p>
        <p>We launched from Leeds Bradford Airport, flying daily to and from Paris. Things really took off for us from here</p>
        <p> we carried 360,000 happy passengers in that first year. By September 2004, Cravendale Travel</p>
        <p> had carried 1 million passengers and we surpassed 20 million passengers in October 2010. In 2012, we carried</p>
        <p> over 4.7 million customers our highest ever total. And everything is still looking up.</p>

        <!--Embeded map to show location of cravendale Travel-->
        <iframe
            src = "https://maps.google.co.uk/maps?q=shipley+west+yorkshire&amp;output=embed">
        </iframe>
        <?include_once('footer.php');?>
    </body>
</html>
