<!DOCTYPE html>
<?php
//FILE TO DISPLAY USER SELECTIONS, ONCE THEY HAVE DECIDED TO CLICK THE LINK "BOOK NOW"
session_start();
include_once('database/open.php');
//MAKING SURE SOMEONE IS LOGGED IN
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    }
?>
<html>
    <head>
        <title>Invoice</title>
    </head>
    <body>
        <?include_once('navigation.php');?>
        <h1>Invoice</h1>
        <?php
        //QUERY TO SELECT ALL THE RELEVANT INFORMATION
            $query ="SELECT
            booking.id as id,
            hotel.name as hotelName,
            destination.city_name as city_name,
            price_band.price as price,
            holiday_type.type as type
            FROM booking INNER JOIN holiday ON booking.holiday_id = holiday.id
            INNER JOIN hotel ON holiday.hotel_id = hotel.id
            INNER JOIN destination ON holiday.destination_id = destination.id
            INNER JOIN price_band ON holiday.price_band_id = price_band.id
            INNER JOIN holiday_type ON holiday.holiday_type_id = holiday_type.id
            WHERE booking.user_id =".$user->id;
            $result = mysqli_query($con,$query);
            if (isset($_SESSION['user'])) {
                $user = $_SESSION['user'];
                if ($result->num_rows > 0) {
                    //WHILE A RESULT PRINT TO SCREEN
                    while($row = mysqli_fetch_assoc($result)) {
                        echo '<form name="holidays">';
                                    echo "<ul>";
                                        echo "<li>";
                                        echo $row['hotelName'];
                                        echo "</li>";
                                        echo "<li>";
                                        echo $row['city_name'];
                                        echo "</li>";
                                        echo "<li>";
                                        echo "&pound;".$row['price'];
                                        echo "</li>";
                                        echo "<li>";
                                        echo $row['type'];
                                        echo "</li>";
                                    echo "</ul>";
                        echo '</form>';
                    }
                } else {
        ?>
              <h3>Your Search Didn't Return Any Results</h3>
        <?
                }
            }
        ?>
        <?include_once('footer.php');?>    
    </body>
</html>   
