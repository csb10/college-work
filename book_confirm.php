<!DOCTYPE html>
<!--FILE TO CONFIRM IF USER WANTS TO BOOK HOLIDAY AND DIRECT TO RELIANT PAGE-->
<?php
session_start();
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    }

    $basket_id = $_GET['basketId'];
?>
<html>
    <head>
        <title>Confirm Booking</title>
    </head>
    <body>
        <?include_once('navigation.php');?>
        <h1>Confirm Booking</h1>
    <?php
        echo "<ul>";
            echo '<li><a href="add_to_checkout.php?basketId='.$basket_id.'">Confirm Booking</a></li>';
            echo "<li>";
            $url = htmlspecialchars($_SERVER['HTTP_REFERER']);
            echo "<a href='$url'>Go Back</a>";
            echo "</li>";
        echo "</ul>";
        include_once('footer.php');
    ?>        
    </body>
</html>

