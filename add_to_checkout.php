<?php
//FILE TO ADD USER SELECTION TO BOOKING TABLE
    session_start();
    include_once('database/open.php');

    if (isset($_GET['basketId']) && $_GET['basketId'] != '') {
        $basket_id = $_GET['basketId'];
    } else header('location: view_basket.php');

    if (isset($_SESSION['user'])) {
        $user = $_SESSION['user'];
    } else header('index.php');

    $query = "SELECT * FROM basket WHERE id = ".$basket_id." AND user_id = ".$user->id;
    $result = mysqli_query($con,$query);
    if ($result->num_rows == 0) {
        header('location: view_basket.php');
    }

    $basket_item = $result->fetch_object();

    $query = "INSERT INTO booking (holiday_id, user_id, departure_date, rooms)
        VALUES (".$basket_item->holiday_id.", ".$basket_item->user_id.", '".$basket_item->departure_date."', ".$basket_item->rooms.")";

    if (mysqli_query($con,$query)) {
        $query = "UPDATE holiday SET available_rooms = (available_rooms - ".$basket_item->rooms.") WHERE id = ".$basket_item->holiday_id;
        if(mysqli_query($con, $query)) {
            $query = "DELETE FROM basket WHERE id = ".$basket_item->id;
            if (mysqli_query($con, $query)) {
                $_SESSION['booked']=1;
                header("location: view_booking.php");
            } else die("Error in query: '".mysqli_error($con)."'");
        } else die("Error in query: '".mysqli_error($con)."'");
    } else echo "ERROR: NO VALUES SET IN QUERY";  
?>
