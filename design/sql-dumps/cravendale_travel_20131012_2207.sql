-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: cravendale_travel
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `basket`
--

DROP TABLE IF EXISTS `basket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `holiday_id` int(11) NOT NULL,
  `rooms` int(11) NOT NULL,
  `departure_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id_idx` (`user_id`),
  KEY `fk_holiday_id_idx` (`holiday_id`),
  CONSTRAINT `fk_holiday_id` FOREIGN KEY (`holiday_id`) REFERENCES `holiday` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `basket`
--

LOCK TABLES `basket` WRITE;
/*!40000 ALTER TABLE `basket` DISABLE KEYS */;
/*!40000 ALTER TABLE `basket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `holiday_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `departure_date` datetime NOT NULL,
  `rooms` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id_idx` (`user_id`),
  KEY `fk_holiday_id_idx` (`holiday_id`),
  CONSTRAINT `fk_booking_holiday_id` FOREIGN KEY (`holiday_id`) REFERENCES `holiday` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` VALUES (60,4,1,'2014-01-03 00:00:00',1),(61,3,1,'2014-01-06 00:00:00',1),(62,3,1,'2014-01-06 00:00:00',1),(63,3,1,'2014-01-06 00:00:00',1),(64,4,1,'2014-01-03 00:00:00',1);
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destination`
--

DROP TABLE IF EXISTS `destination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destination`
--

LOCK TABLES `destination` WRITE;
/*!40000 ALTER TABLE `destination` DISABLE KEYS */;
INSERT INTO `destination` VALUES (1,'London'),(2,'Edinburgh'),(3,'Paris');
/*!40000 ALTER TABLE `destination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `holiday`
--

DROP TABLE IF EXISTS `holiday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `holiday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `price_band_id` int(11) NOT NULL,
  `holiday_type_id` int(11) NOT NULL,
  `available_rooms` int(11) NOT NULL DEFAULT '15',
  PRIMARY KEY (`id`),
  KEY `fk_hotel_id_idx` (`hotel_id`),
  KEY `fk_destination_id_idx` (`destination_id`),
  KEY `fk_price_band_id_idx` (`price_band_id`),
  KEY `fk_holiday_type_id_idx` (`holiday_type_id`),
  CONSTRAINT `fk_destination_id` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_holiday_type_id` FOREIGN KEY (`holiday_type_id`) REFERENCES `holiday_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_hotel_id` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_price_band_id` FOREIGN KEY (`price_band_id`) REFERENCES `price_band` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `holiday`
--

LOCK TABLES `holiday` WRITE;
/*!40000 ALTER TABLE `holiday` DISABLE KEYS */;
INSERT INTO `holiday` VALUES (1,1,1,2,1,13),(2,2,1,2,1,14),(3,2,1,4,2,13),(4,3,2,1,1,9),(5,4,2,2,1,15),(6,5,2,2,1,15),(7,3,2,3,2,15),(8,4,2,3,2,15),(9,6,3,3,1,13),(10,7,3,3,1,14),(11,6,3,4,2,15),(12,7,3,4,2,15);
/*!40000 ALTER TABLE `holiday` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `holiday_type`
--

DROP TABLE IF EXISTS `holiday_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `holiday_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `holiday_type`
--

LOCK TABLES `holiday_type` WRITE;
/*!40000 ALTER TABLE `holiday_type` DISABLE KEYS */;
INSERT INTO `holiday_type` VALUES (1,'Weekend Break'),(2,'Midweek Break');
/*!40000 ALTER TABLE `holiday_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotel`
--

DROP TABLE IF EXISTS `hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `house_number` varchar(45) NOT NULL,
  `address_1` varchar(45) NOT NULL,
  `address_2` varchar(45) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `county` varchar(45) NOT NULL,
  `postcode` varchar(45) NOT NULL,
  `country` varchar(45) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `rooms` int(11) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel`
--

LOCK TABLES `hotel` WRITE;
/*!40000 ALTER TABLE `hotel` DISABLE KEYS */;
INSERT INTO `hotel` VALUES (1,'The Marylebone Hotel ','47','Welbeck Street,',NULL,'London','London','W1G 8DN ','United Kingdom','0207 486 6600','marylebone@doylecollection.com',200,'The Marylebone Hotel is one of the finest hotels in London - a haven of understated and contemporary luxury that turns even the briefest of stays into a genuine pleasure.\r\nThis renowned London city hotel is located right in the heart of the Marylebone district, which has managed to retain a charming, village feel that you\'ll find nowhere else in this great city.\r\nStylish shopping streets are home to a wonderful selection of quirky shops and boutiques that are unique to Marylebone. And walk just slightly further afield and you\'re sampling the delights of Oxford Circus or Bond Street, or perhaps making some time for yourself in the magnificent Hyde Park.\r\nAnd when you return to The Marylebone Hotel after your day\'s exploring, you\'ll find a level of delightfully informal and unfussy service from a team who value your stay with us just as much as you yourself do. This service is extended in our health club too. Here you can use state of the art gym equipment, swim in an 18 meter pool or relax with a Payot treatment in the spa, all of which is delivered by the upscale operator Third Space.\r\nSo whether for business or pleasure, make your next trip to London an experience where you can savour the soul of a village in the heart of the city.','http://the-luxelife.com/wp-content/uploads/2012/08/edit8.jpg'),(2,'The Gainsborough Hotel','7','Queensberry Place',NULL,'London','London','SW7 2DL','United Kingdom','020 7957 0000','info@thegainsboroughhotel.com',500,'The Gainsborough Hotel is located in the heart of London in South Kensington, making it the ideal base from which to discover the fashionable restaurants, antique markets, pubs, and the beautiful backwaters of Chelsea and Knightsbridge. With easy access to all the enjoyable activities and monuments that London has to offer visitors The Gainsborough Hotel is the ideal accommodation choice for both the business and leisure guest.\r\n\r\nEffortlessly combining the style of bygone days and modern times, The Gainsborough Hotel boasts four star qualities in an English Country Home in London Borough. Located close to Kensington Palace, Kensington Gardens, numerous museums, art galleries, and live entertainment, The Gainsborough Hotel is perfect for those wanting to explore the beautiful sights of Kensington, London.','http://exp.cdn-hotels.com/hotels/1000000/490000/486300/486279/486279_47_b.jpg'),(3,'The George Hotel','19','George Street',NULL,'Edinburgh','Edinburgh','EH2 2PB','United Kingdom','0131 225 1251','info@georgehotel.com',249,'The George Hotel is the pinnacle of luxury hotels in Edinburgh and proud winner of the Best Hotel in Edinburgh at the Scottish Hotel Awards 2012. This leading 4 star, 18th century hotel is centrally located in the beautiful and historic city of Edinburgh and is easily accessible from Edinburgh International Airport, all major transport links and just minutes from Waverley Train Station.','http://www.entirescotland.com/edinburgh/george_hotel_edinburgh.jpg'),(4,'Tigerlily','125','George Street',NULL,'Edinburgh ','Edinburgh ','EH2 4JN','United KIngdom','0131 225 5005','info@tigerlilyedinburgh.co.uk',33,'Stylish. Contemporary. Indulgent. Just a few words that sum up Tigerlily, a luxurious boutique hotel in the heart of Edinburgh\'s city centre.\r\n\r\nSet back from the hustle and bustle in one of the city\'s most fashionable districts for shopping and dining, make Tigerlily your perfect base for a leisurely Edinburgh city break, romantic weekend away or simply some much needed rest and relaxation with friends and family.','http://static.mightydeals.co.uk/images/products/974/Big.jpg'),(5,'The Witchery by the Castle','1','Castlehill','The Royal Mile','Edinburgh','Edinburgh','EH1 2NF','United Kingdom','0131 225 5613','info@thewitchery.com',100,'The Witchery is located at the top of Edinburgh\'s historic Royal Mile. You\'ll see us on the left hand side of the road as you approach Edinburgh Castle','http://weburbanist.com/wp-content/uploads/2011/06/UK-hotels-The-Witchery-Edinburgh.jpg'),(6,'Hotel Montpensier','12','rue de Richelieu',NULL,'Paris','Paris','75001','France',' 331 42 96 28 50','hotel-montpensier@wanadoo.fr',43,'The hotel Montpensier Paris - France - is happy to accomodate you in full heart of Paris. Ideal for your tourist stays or of businesses (Direct line with the greatest business district of Europe: Paris Defense).\r\n \r\nFor your comfort the hotel montpensier places at your disposal, in the hall of reception, a free access WIFI. The 43 hotel rooms Montpensier Paris are equipped with the Internet high flow.','http://aff.bstatic.com/images/hotel/max500/206/20617404.jpg'),(7,'Albe Hotel','1','rue de la Harpe',NULL,'Paris','Paris','75005','France','33 1 46 34 09 70','albehotel@wanadoo.fr',50,'Welcome to the elegant Albe Hotel, a three-star hotel in Paris with a chic, contemporary decor located right by the legendary Boulevard Saint-Michel. Come and drink in the unique atmosphere of Paris’s Left Bank with its bohemian flavour while also savouring the many classical Parisian sights near the hotel.','http://www.hoteldesignquartierlatinparis.com/wp-content/uploads/2011/02/Hotel-Albe-Paris-1024x315.jpg');
/*!40000 ALTER TABLE `hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_band`
--

DROP TABLE IF EXISTS `price_band`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_band` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `band` varchar(45) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_band`
--

LOCK TABLES `price_band` WRITE;
/*!40000 ALTER TABLE `price_band` DISABLE KEYS */;
INSERT INTO `price_band` VALUES (1,'A',160),(2,'B',180),(3,'C',240),(4,'D',280);
/*!40000 ALTER TABLE `price_band` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `house_number` varchar(45) NOT NULL,
  `address_1` varchar(45) NOT NULL,
  `address_2` varchar(45) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `postcode` varchar(45) NOT NULL,
  `country` varchar(45) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Mr','Craig','Bentall','10','Beacon Road',NULL,'Bradford','BD6 3DF','United Kingdom','01423 500900','craig@','craig','1234'),(2,'Mr','Gordon','Brown','34','Park Lane',NULL,'Leeds','LS3 6GF','United KIngdom','077456789','Gordon@','Gordon','1234'),(6,'mr','Joe','Bloggs','1','Road','','Bradford','BD5 6HG','england','01234568','joe@','Joe','1234'),(7,'mr','','','','','','','','please select country','','','john',''),(8,'mrs','kelly','Bentall','351 ','Rose Garden','','York','Y76 E34','england','12345689','kelly@kelly','kelly','1234');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-10 22:07:31
