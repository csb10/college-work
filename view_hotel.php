<!DOCTYPE html>
<?php
//FILE TO DISPLAY MORE INFORMATION ABOUT A PIRTICULAR HOTEL
session_start();
include_once('database/open.php');

//CHECKING TO SEE IF HOTELID IS SET, IF IS SET DO A QUERY AND ACCES ALL HOTEL INFORMATION
if (isset($_GET['hotelId'])) {

    $hotelId = $_GET['hotelId'];
    $result = mysqli_query($con, "SELECT * FROM hotel WHERE id = ".$hotelId);
    if ($result->num_rows == 0) header('Location: index.php');
    $hotel = $result->fetch_object();

} else {
    header('Location: index.php');
}

//CHECK IF NUMBER OF ROOMS SET
if (isset($_GET['room'])) {
    $room = $_GET['room'];
} else $room = 1;

//CHECK IF THERE A HOLIDAY ID
if (isset($_GET['holidayId'])) {  
    $holidayId = $_GET['holidayId'];
}

?>
<html>
    <head>
        <title>View Hotel - <?=$hotel->name;?></title>
    </head>
    <body>
        <?include_once('navigation.php');?>
        <h1>Hotel Details</h1>
        <form name='bookingform' action='add_to_basket.php' method='GET'>
            <ul>
       <!--/IF THERES A HOLIDAY ID, DISPLAY LINK SO THE USER CAN ADD TO BASKET-->
        <?php
            if (isset($holidayId)) {
                echo '<a href="add_to_basket.php?holidayId='.$holidayId.'">Add to Basket</a></li>';
            }
        ?>
            </ul>
        </form>
<!--DISPALY HOTEL INFORMATION-->
        <ul>
            <li><h2><?=$hotel->name;?></h2></li>
            <li> <img alt="Hotel"  src="<?=$hotel->image;?>" /></li>
            <li><h2>Description</h2></li>   
            <li> <p><?=$hotel->description;?></p></li>
            <li><h2>Find us</h2></li>
            <li><?=$hotel->house_number;?></li>
            <li><?=$hotel->address_1;?></li>
            <? if ($hotel->address_2 != '') : ?>
            <li><?=$hotel->address_2;?></li>
            <? endif ?>
            <li><?=$hotel->city;?></li>
            <li><?=$hotel->county;?></li>
            <li><?=$hotel->postcode;?></li>
            <li><?=$hotel->country;?></li>       
            <li><h2>Contact Us</h2></li>     
            <li><label>Telephone: </label><?=$hotel->telephone;?></li>
            <li><label>E-Mail: </label><a href="mailto:<?=$hotel->email;?>"><?=$hotel->email;?></a></li>
        </ul>
        <?include_once('footer.php');?>    
    </body>
</html>
<?php
include_once('database/close.php');
?>
