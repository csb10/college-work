<?php
//FILE TO ADD USERS SELECTION TO THE BASKET
session_start();
include_once('database/open.php');

$holiday = $_GET['holidayId'];
$user = $_SESSION['user'];

//CHECK IF ALL RELEVANT INFORMATION IS SET
if (isset($_SESSION['departure_date'], $_SESSION['room'])) {
    $departure_date = $_SESSION['departure_date'];
    $room = $_SESSION['room'];
} else {
    header("Location: index.php");
}

// AND IF USER IS LOGGED IN
//THEN INSERT VALUES INTO BASKET TABLE
if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
    
    $query = "INSERT INTO basket(user_id, holiday_id, rooms, departure_date)
     VALUES($user->id, $holiday, $room, '".$departure_date."')";
     if (mysqli_query($con,$query)) {
         $_SESSION['basket_add']=1; 
         header("location: view_basket.php");
     } else {
         echo"ERROR: NO VALUES SET IN QUERY"; 
     }

} else {

    header("location: login_form.php");
}

?>

