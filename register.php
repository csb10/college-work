<?php
//FILE TO REGISTER THE USER TO THE DATABASE
session_start();
include_once('database/open.php');

    //Assign variables
    $username           = $_REQUEST["username"];
    $username_confirm   = $_REQUEST["username_confirm"];
    $password           = $_REQUEST["password"];
    $password_confirm   = $_REQUEST["password_confirm"];
    $title              = $_REQUEST["title"];
    $firstname          = $_REQUEST["firstname"];
    $lastname           = $_REQUEST["lastname"];
    $house_number       = $_REQUEST["house_number"];
    $address_1          = $_REQUEST["address_1"];
    $address_2          = $_REQUEST["address_2"];
    $city               = $_REQUEST["city"];
    $postcode           = $_REQUEST["postcode"];
    $country            = $_REQUEST["country"];
    $telephone          = $_REQUEST["telephone"];
    $email              = $_REQUEST["email"];
    //////////////////////////////////////////////

    //DECLARE VAILD AS TURE - ASSUME EVERYTING IS FINE
    //UNTIL WE FIND SOME INPUT THAT IS INVALID    
    $valid = true;
    //VALIDATE USERNAME
    if ($username != '') {
       if ($username == $username_confirm) {
            $query = "SELECT id FROM user WHERE username LIKE '".$username."'";
            $user_count = mysqli_query($con,$query);
            if ($user_count->num_rows > 0) {
                $valid = false;
                $error_message = "Username provided is not unique";
                echo $error_message."<br/>";
            }
       } else {
            $valid = false;
            $error_message = "Username and Confirm Username do not match ";
            echo $error_message."<br/>";
       }
    } else {
        $valid = false;
        $error_message = "No Username provided";
        echo $error_message."<br/>";
    }

    //VALIDATE PASSWORD
    if ($password != '') {
        if ($password != $password_confirm) {
            $valid = false;
            $error_message = "The Password and Confirm Password provided do not match";
            echo $error_message."<br/>";
        }
    } else {
        $valid = false;
        $error_message = "No password provided";
        echo $error_message."<br/>";
    }
  
    //VALIDATE FIRSTNAME
    if ($firstname == '') {
        $valid = false;
        $error_message = "No Firstname provided";
        echo $error_message."<br/>";
    }
        
    //VALIDATE LASTNAME
    if ($lastname == '') {
        $valid = false;
        $error_message = "No Lastname provided";
        echo $error_message."<br/>";
    }
        
    //VALIDATE HOUSE_NUMBER
    if ($house_number == '') {
        $valid = false;
        $error_message = "No House Number provided";
        echo $error_message."<br/>";
    }
    
    //VALIDATE ADDRESS_1
    if ($address_1 == '') {
        $valid = false;
        $error_message = "No Address1 provided";
        echo $error_message."<br/>";
    }
    
    //VALIDATE CITY
    if ($city == '') {
        $valid = false;
        $error_message = "No City provided";
        echo $error_message."<br/>";
    }
        
    //VALIDATE POSTCODE
    if ($postcode == '') {
        $valid = false;
        $error_message = "No Postcode provided";
        echo $error_message."<br/>";
    }
        
    //VALIDATE EMAIL
    if ($email == '') {
        $valid = false;
        $error_message = "No Email provided";
        echo $error_message."<br/>";
    }  

    //PERFORM QUERY IF EVERYTHING IS OK, QUERY ADD'S VALUES TO DATABASE
    if ($valid) {
        $query = "INSERT INTO user(title, firstname, lastname, house_number, address_1, address_2, city, postcode, country, telephone, email, username, password)
        VALUES('$title', '$firstname', '$lastname', '$house_number', '$address_1', '$address_2', '$city', '$postcode', '$country', '$telephone', '$email', '$username', '$password')";
        $user = mysqli_query($con,$query);

        header("location: index.php");
    } else {
        echo '<br/><a href="javascript:history.go(-1)">[Go Back]</a>';
    }
?>
