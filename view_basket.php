<!DOCTYPE html>
<?php
//FILE FOR DISPLAYING BASKET
session_start();
include_once('database/open.php');

    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        header('location: index.php');
    }
?>
<html>
    <head>
        <title>Basket</title>
    </head>
    <body>
    <!-- QUERY TO GET INFORMATION FOR THE BASKET -->
        <?include_once('navigation.php');?>
        <h1>Basket</h1>
        <?php
            $query ="SELECT
            basket.id as id,
            hotel.name as hotelName,
            destination.city_name as city_name,
            price_band.price as price,
            holiday_type.type as type,
            basket.rooms as rooms,
            holiday.id as holidayId,
            DATE_FORMAT(basket.departure_date, '%W %D %M, %Y') as departure_date
            FROM basket INNER JOIN holiday ON basket.holiday_id = holiday.id
            INNER JOIN hotel ON holiday.hotel_id = hotel.id
            INNER JOIN destination ON holiday.destination_id = destination.id
            INNER JOIN price_band ON holiday.price_band_id = price_band.id
            INNER JOIN holiday_type ON holiday.holiday_type_id = holiday_type.id
            WHERE basket.user_id =".$user->id;
            $result = mysqli_query($con,$query);
// IF A USER IS SET AND MATCHES FOUND, PRINT TO SCREEN
            if (isset($_SESSION['user'])) {
                $user = $_SESSION['user'];
                if ($result->num_rows > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        echo '<form name="holidays">';
                                    echo "<ul>";
                                        echo "<li>";
                                        echo $row['hotelName'];
                                        echo "</li>";
                                        echo "<li>";
                                        echo $row['city_name'];
                                        echo "</li>";
                                        echo "<li>";
                                        echo "&pound;".$row['price'];
                                        echo "</li>";
                                        echo "<li>";
                                        echo $row['type'];
                                        echo "</li>";
                                        echo "<li>";
                                        echo $row['departure_date'];
                                        echo "</li>";
                                        echo "<li>";
                                        echo "Rooms:".$row['rooms'];
                                        echo "</li>";
                                        echo '<li><a href="delete_from_basket.php?id='.$row['id'].'">Delete</a></li>';
                                        echo '<li><a href="book_confirm.php?basketId='.$row['id'].'">Book Now</a></li>';
                                     echo "</ul>";
                        echo '</form>';
                    }
                } else {
        ?>
              <h3>Your Basket Is Empty</h3>
        <?
                }
            }
        ?>
        <?include_once('footer.php');?>    
    </body>
</html>
