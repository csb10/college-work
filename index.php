<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
    </head>
    <body>
<?php
//FILE FOR USER TO INSTIGATE A SEARCH
session_start();
include_once('database/open.php');

//CHECK IF USER IS LOGGED IN
if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
    $user = $_SESSION['user'];
    echo "Welcome Back ".$user->firstname." ".$user->lastname;
}

//IF SET PRINTS MESSGAE TO SCREEN, THEN UNSETS ITSELF
if (isset($_SESSION['basket_add'])) {
    echo "&nbsp;Holiday Added To Basket";
    unset($_SESSION['basket_add']);
}

if (isset($_SESSION['booked'])) {
        echo "&nbsp;Holiday Booked";
        unset($_SESSION['booked']);
}

if (isset($_SESSION['logged_out'])) {
        echo "&nbsp;You have been logged out";
        unset($_SESSION['logged_out']);
}

////CODE FOR SENDING USER BACK TO SEARCH SCREEN, IF BEEN REDIRECT HERE AFTER TRYING TO ADD TO BASKET WITHOUT BEING LOGGED IN 
//CODE TO GENERATE BOOKING DATES
    $start_date = "2014-01-01"; //Format must be YYYY-MM-DD
    $end_date = "2014-01-17"; //Format must be YYYY-MM-DD
    $current_date = $start_date;

    if ($start_date > $end_date) {
        die("Problem with dates - ".$start_date." is greater than ".$end_date);
    }

    $i = 1;
    while ($current_date <= $end_date) {
        $current_date = date("Y-m-d", strtotime("+".$i." day", strtotime($start_date)));
        $current_date_timestamp = strtotime($current_date);

        if (get_day_number($current_date) == 1 || get_day_number($current_date) == 5) {
            $current_date_label = date("D jS M, Y", $current_date_timestamp); 
            $departure_dates[] = array("value" => $current_date, "label" => $current_date_label);
        }
        $i++;
    }
?>   
        <?include_once('navigation.php');?>
        <h1>Home</h1>
        <h3>Search Form</h3>
<?php
//FORM TO DISPLAY DESTINATIONS
//BY LOOPING THROUGH DATABASE AND ENDING WHEN NO RESULTS LEFT

    echo "<form name='searchform' action='search.php' method='GET'>";
    echo "<pre>    Destination: ";
    echo "<select name='destination'>";
    $result = mysqli_query($con,"SELECT * FROM destination");
    while($row = mysqli_fetch_assoc($result)) {
            echo '<option value="'.$row['id'].'">'.$row['city_name'].'</option>';
        }
        echo "</select>";
        echo"</pre>";

//DISPLAY PRICE BANDS
    echo "<pre>     Price Band: ";
    echo "<select name='price_band'>";
    $result = mysqli_query($con,"SELECT * FROM price_band");
    while($row = mysqli_fetch_assoc($result)) {
        echo '<option value="'.$row['id'].'">'.$row['band'].'&nbsp; &pound;'.$row['price'].'</option>';
        }
        echo "</select>";
        echo"</pre>";
?>
<!--DISPLAY DEPARTURE DATES-->
    <pre>Departure Dates: <select name="departure_date">
    <? foreach ($departure_dates as $departure_date) { ?>
        <option value="<?=$departure_date['value'];?>"><?=$departure_date['label'];?></option>
    <? } ?>
    </select></pre>
<!--DISPLAY ROOMS-->                                                                        
    <pre>          Rooms: <select name="room">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
    </select></pre>

    <pre>                 <input id="submit" type="submit" value="submit"></pre>
    </form>          
       
        <?include_once('footer.php');?>
    </body>
</html>
