<?php
//FILE TO DISPLAY THE NAVIGATION BAR AND LINKS TO THE RELEVANT PAGES

//IF A USER ID LOGGED IN, NAVIGATION BAR WILL SHOW THEIR LASTNAME AND LOGOUT
if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
    $userLoggedIn = true;
    $user = $_SESSION['user'];
}else $userLoggedIn = false;

echo 
"<nav id='navigation_bar'>
    <ul>";
if ($userLoggedIn) {
    echo '
        <li>'.$user->title.'&nbsp;'.$user->lastname.' (<a href="logout.php">Logout</a>)</li>
        <li><a href="view_basket.php">Basket</a></li>';
} else echo '<li><a href="login_form.php">Login/Register</a></li>';

echo '
       <li><a href="index.php">Home</a></li>
       <li><a href="hotels.php">Hotels</a></li>
    </ul>
</nav>
';

?>
