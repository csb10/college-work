<!DOCTYPE html>
<?php
//FILE TO DIPLAY ALL THE HOTELS IN THE DATABASE
session_start();
include_once('database/open.php');
?>
<html>
    <head>
        <title>Hotels Page</title>
    </head>
    <body>
        <?include_once('navigation.php');?>
<?php
//SELECT ALL DETAILS FROM HOTEL TABLE AND WHILE THERE ARE RESULTS PRINT TO SCREEN
$result = mysqli_query($con,"SELECT * FROM hotel");
while($row = mysqli_fetch_assoc($result))
    {
        echo "<ul>";
            echo "<li>";
            echo $row['name'];
            echo "<br>";
            echo "</li>";
            echo "<li>";
            $image = $row['image'];
            echo '<a href="view_hotel.php?hotelId='.$row['id'].'"><img alt="Hotel" src="' . $image . '"></a>';
            echo "</li>";
        echo "</ul>";    
    }

?>
        
        <?include_once('footer.php');?>    
    </body>
</html>
