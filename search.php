<!DOCTYPE html>
<?php
//FILE TO SHOW SEARCH RESULTS
session_start();
include_once('database/open.php');

    //ASSIGN VARIABLES
    $destination             = $_GET['destination'];
    $price_band              = $_GET['price_band'];
    $departure_date          = $_GET['departure_date'];
    $room                    = $_GET['room'];

    $_SESSION['departure_date'] = $departure_date;
    $_SESSION['room'] = $room;

    $departure_day = get_day_number($departure_date);

    if ($departure_day != 1 && $departure_day != 5) {
        header("location: index.php");
    } else if ($departure_day == 1) {
        $holiday_type = MIDWEEK_BREAK;
    } else if ($departure_day == 5) {
        $holiday_type = WEEKEND_BREAK;
    }
 ?>
<html>
    <head>
        <title>Search Page</title>
    </head>
    <body>
    <!--INCLUDING NAVIGATION LINKS -->
        <?include_once('navigation.php');?>
<?php
    //QUERY TO SELECT HOTELS THAT MATCH USERS SELECTION

    $query = 
    "
    SELECT 
     holiday.id as holidayId,
     hotel.id as hotelId,
     hotel.name as hotelName,
     hotel.image as hotelImage
    FROM 
     holiday 
    INNER JOIN 
     hotel 
     ON 
     holiday.hotel_id = hotel.id 
    INNER JOIN 
     destination 
     ON holiday.destination_id = destination.id 
    INNER JOIN 
     price_band 
     ON 
     holiday.price_band_id = price_band.id 
    INNER JOIN 
     holiday_type 
     ON 
     holiday.holiday_type_id = holiday_type.id 
    WHERE 
     destination.id = ".$destination."
    AND 
     price_band.id = ".$price_band."
    AND 
     holiday_type.id = ".$holiday_type."
    AND 
     holiday.available_rooms >= ".$room;

    $result = mysqli_query($con,$query);

    if ($result->num_rows > 0) {

    ?>
        <h3>Your Search Results</h3>
    <?
    //WHILE THERE ARE RESULTS, DISPLAY TO THE PAGE
        while($row = mysqli_fetch_assoc($result))
        {
        echo '<form name="hotels" action="add_to_basket.php" method="GET">';
            echo "<ul>";
                echo "<li>";
                echo $row['hotelName'];
                echo "<br>";
                echo "</li>";
                echo "<li>";
                echo '<a href="view_hotel.php?hotelId='.$row['hotelId'].'&amp;holidayId='.$row['holidayId'].'" target="_blank"><img alt = "Hotel" src="' . $row['hotelImage'] . '"></a>';
                echo "</li>";
                echo '<li><a href="add_to_basket.php?holidayId='.$row['holidayId'].'">Add to Basket</a></li>';
            echo "</ul>";    
        echo '</form>';
        }
    } else {
        ?>
        <h3>Your Search Didn't Return Any Results</h3>
        <?
    }
?>
    <?include_once("footer.php");?>
    </body>
</html>
